# Famous places near me

Developing an Android application that contacts with a restful API to get the most popular and famous places around you while you're walking in the town. In addition to Developing the restful API that contacts with Wikipedia to get information about the place.

This project was the developed at [Asal technologies](https://www.asaltech.com/) as my training project.

Used technologies \[*Xamarin.Android + PHP*\]