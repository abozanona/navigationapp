using SQLite;

namespace NavigationApp.Classes
{
    /// <summary>
    /// Single place item class
    /// </summary>
    public class Place {
        [PrimaryKey]
        public int placeId { set; get; }
        public int ID { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string placeName { set; get; }
        public string placeType { set; get; }
    }
}