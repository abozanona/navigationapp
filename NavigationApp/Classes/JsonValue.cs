using System;
using System.Collections.Generic;
using System.Net;
using System.IO;


namespace NavigationApp.Classes
{
    #region ErrorFetchingDataException
    public class ErrorFetchingDataException : Exception {
        public ErrorFetchingDataException() {
        }

        public ErrorFetchingDataException(string message)
            : base(message) {
        }

        public ErrorFetchingDataException(string message, Exception inner)
            : base(message, inner) {
        }
    }
    #endregion

    public class JsonValue {
        /// <summary>
        /// Fetching Json from url
        /// </summary>
        /// <param name="url">The url to fetch json from</param>
        /// <param name="GETParam">GET parameters of the url</param>
        /// <returns></returns>
        public static string FetchJson(string url, Dictionary<string, string> GETParam) {
            url += "?";
            foreach (KeyValuePair<string, string> item in GETParam) {
                url += GETParam.Keys + "=" + item.Value + "&";
            }
            url = System.Uri.EscapeDataString(url);

            WebRequest request;
            try {
                request = HttpWebRequest.Create(url);
            }
            catch
            {
                Console.WriteLine("Error parsing error");
                return "";
            }
            request.ContentType = "application/json";
            request.Method = "GET";

            try { 
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse) {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new ErrorFetchingDataException();
                    using (StreamReader reader = new StreamReader(response.GetResponseStream())) {
                        var content = reader.ReadToEnd();
                        if (string.IsNullOrWhiteSpace(content)) {
                            throw new ErrorFetchingDataException();
                        } else {
                            return content;
                        }

                        //Assert.NotNull(content);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Error in connection.");
                return "";
            }
        }
    }
}