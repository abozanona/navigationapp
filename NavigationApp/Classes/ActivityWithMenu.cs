
using Android.App;
using Android.Views;
using Xamarin.Facebook.Login;

namespace NavigationApp
{
    /// <summary>
    /// adding the navigation menu to activities
    /// </summary>
    public class ActivityWithMenu : Activity {
        public override bool OnPrepareOptionsMenu(IMenu menu) {
            MenuInflater.Inflate(Resource.Menu.main_option_menu, menu);
            return base.OnPrepareOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item) {
            switch (item.ItemId) {
                case Resource.Id.mnu_settings:
                    StartActivity(typeof(Settings));
                    Finish();
                    return true;
                case Resource.Id.mnu_about:
                    StartActivity(typeof(About));
                    Finish();
                    return true;
                case Resource.Id.mnu_logout:
                    LoginManager.Instance.LogOut();
                    StartActivity(typeof(MainActivity));
                    Finish();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}