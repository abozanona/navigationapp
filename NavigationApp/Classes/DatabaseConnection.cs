using Org.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NavigationApp.Classes
{
    class GPSPoint
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class DatabaseConnection
    {
        private static SQLiteConnection conn;

        public DatabaseConnection()
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            conn = new SQLiteConnection(System.IO.Path.Combine(folder, "navigationApp.db"));

            conn.DropTable<GPSPoint>();
            conn.DropTable<Place>();

            conn.CreateTable<GPSPoint>();
            conn.CreateTable<Place>();

        }

        public static int addGPSPoint(double lat, double lng, JSONArray jArray)
        {
            GPSPoint gpsPoint = new GPSPoint { lat = lat, lng = lng };
            conn.Insert(gpsPoint);
            for(int i = 0; i < jArray.Length(); i++)
            {
                Place place = new Place
                {
                    ID = gpsPoint.ID,
                    lat = jArray.GetJSONObject(i).GetString("lat"),
                    lng = jArray.GetJSONObject(i).GetString("lng"),
                    placeId = jArray.GetJSONObject(i).GetInt("placeId"),
                    placeName = jArray.GetJSONObject(i).GetString("placeName"),
                    placeType = jArray.GetJSONObject(i).GetString("placeType")                    
                };
                
                //checked already
                //int count = conn.ExecuteScalar<int>("SELECT COUNT(*) FROM Place WHERE lat=" + place.lat + " AND lng=" + place.lng);
                //if (count == 0)
                conn.Insert(place);

                return place.ID;
            }
        }

        public static List<Place> searchForPlace(double lat, double lng)
        {
            //var gpsPoints = conn.Table<GPSPoint>();
            var query = conn.Query<GPSPoint>(string.Format("SELECT * FROM GPSPoint WHERE ({0}-lat)*({0}-lat) + ({1}-lng)*({1}-lng) < 25", lat, lng));//.Where(v => Math.Sqrt(Math.Pow(v.lat - lat, 2) - Math.Pow(v.lng - lng, 2)) < 5);

            List<Place> l = new List<Place>();


            if (query.Count != 0) 
            {
                var queryPlaces = conn.Query<Place>(string.Format("SELECT * FROM Place WHERE ID = {0}", query[0].ID));//.Where(v => v.ID == gpsPoint.ID);
                int t = queryPlaces.Count;
                foreach (var place in queryPlaces) 
                    l.Add(place);
            }
            return l;
        }

    }
}