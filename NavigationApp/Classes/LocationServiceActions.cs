using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Locations;
using System.Threading.Tasks;
using Org.Json;
using Android.Preferences;
using NavigationApp.Classes.Services;
using System;

namespace NavigationApp.Classes
{
    [Service]
    public class LocationServiceActions
    {
        private int inRangePlaces;
        //private List<Place> inRangePlaces;
        private static readonly int GPSNotificationID = 1000;
        public static Location currentLocation;
        public delegate void callBackDelegate();
        //callback of OnLocationChanged
        public callBackDelegate callBack;
        private Context mContext;

        private LocationManager locationManager;

        public LocationServiceActions()
        {
            mContext = Application.Context;
            InitializeLocationManager();
            callBack = null;
            //inRangePlaces = new List<Place>();
            callBack += checkServerForNearByPlaces;
        }
        #region location

        string locationProvider;
        private void InitializeLocationManager()
        {
            locationManager = Application.Context.GetSystemService("location") as LocationManager;
            Criteria criteriaForLocationService = new Criteria
            {
                Accuracy = Accuracy.Fine
            };
            IList<string> acceptableLocationProviders = locationManager.GetProviders(criteriaForLocationService, true);

            if (acceptableLocationProviders.Any())
                locationProvider = acceptableLocationProviders.First();
            else
                locationProvider = string.Empty;
        }
        

        #endregion
        
        private void checkServerForNearByPlaces()
        {
            if (currentLocation == null)
                return;

            List<Place> savedPlaces = DatabaseConnection.searchForPlace(currentLocation.Latitude, currentLocation.Longitude);
            if (savedPlaces.Count != 0)
            {
                //for (int i = 0; i < savedPlaces.Count; i++)
                //{
                //    if (!inRangePlaces.Exists(x => x.placeId == savedPlaces[i].placeId))
                //    {
                //        inRangePlaces.Add(savedPlaces[i]);
                //        notification(savedPlaces[i]);
                //    }
                //}

                if (inRangePlaces!= savedPlaces[0].ID)
                {
                    for (int i = 0; i < savedPlaces.Count; i++)
                    {
                        notification(savedPlaces[i]);
                    }
                    inRangePlaces = savedPlaces[0].ID;
                }


                return;
            }
            //get saves settings
            ISharedPreferences sp = PreferenceManager.GetDefaultSharedPreferences(mContext);
            string settings = sp.GetString("preferredSettings", "{}");

            string url = mContext.Resources.GetString(Resource.String.server_url);
            Dictionary<string, string> d = new Dictionary<string, string>();
            d["searchByLocation"] = currentLocation.Latitude + "";
            d["lat"] = currentLocation.Latitude + "";
            d["lng"] = currentLocation.Longitude + "";
            d["settings"] = settings;
            string content = JsonValue.FetchJson(url, d);
            content = @"[{""placeId"":1, ""placeName"":""Al-Aqsa mosque"", ""placeType"":""wfqfqwf"", ""lat"":""30"", ""lng"":""50""}]";
            JSONArray jObj;
            try
            {
                jObj = new JSONArray(content);
            }
            catch
            {
                Toast.MakeText(mContext, mContext.Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
                jObj = new JSONArray("[]");
            }



            inRangePlaces = DatabaseConnection.addGPSPoint(currentLocation.Latitude, currentLocation.Longitude, jObj);

            
            for (int i = 0; i < jObj.Length(); i++)
            {
                JSONObject obj = jObj.GetJSONObject(i);
                Place place = new Place();
                place.placeId = obj.GetInt("placeId");
                place.placeName = obj.GetString("placeName");
                place.placeType = obj.GetString("placeType");

                //inRangePlaces.Add(place);
                notification(place);
            }

        }

        #region notification
        private void notification(Place place)
        {
            //https://developer.xamarin.com/guides/cross-platform/application_fundamentals/notifications/android/local_notifications_in_android_walkthrough/

            // When the user clicks the notification, SecondActivity will start up.
            Intent resultIntent = new Intent(mContext, typeof(AboutPlace));

            // Pass some values to SecondActivity:
            resultIntent.PutExtra("placeId", place.placeId);
            TaskStackBuilder stackBuilder = TaskStackBuilder.Create(mContext);
            stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(AboutPlace)));
            stackBuilder.AddNextIntent(resultIntent);

            // Create the PendingIntent with the back stack:            
            PendingIntent resultPendingIntent =
                stackBuilder.GetPendingIntent(0, PendingIntentFlags.UpdateCurrent);

            Android.Support.V4.App.NotificationCompat.Builder builder = new Android.Support.V4.App.NotificationCompat.Builder(mContext)
               //.SetAutoCancel(true)                    // Dismiss from the notif. area when clicked
               .SetContentIntent(resultPendingIntent)  // Start 2nd activity when the intent is clicked.
               .SetContentTitle("You have a new place near you.")      // Set its title
                                                                       //.SetNumber(count)                       // Display the count in the Content Info
                                                                       //.SetSmallIcon(Resource.Drawable.ic_stat_button_click)  // Display this icon
               .SetContentText(place.placeName);             // The message to display.

            // Finally, publish the notification:
            NotificationManager notificationManager =
                (NotificationManager)mContext.GetSystemService(Context.NotificationService);
            notificationManager.Notify(GPSNotificationID, builder.Build());

        }

        public static implicit operator LocationServiceActions(LocationService v)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}