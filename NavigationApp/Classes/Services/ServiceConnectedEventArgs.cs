using System;
using Android.OS;

namespace NavigationApp.Classes.Services
{
    public class ServiceConnectedEventArgs : EventArgs
    {
        public IBinder Binder { get; set; }
    }
}