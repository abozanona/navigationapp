using Android.OS;

namespace NavigationApp.Classes.Services
{
    public class LocationServiceBinder : Binder
    {
        public LocationService Service
        {
            get { return service; }
        }
        protected LocationService service;

        public bool IsBound { get; set; }

        // constructor
        public LocationServiceBinder(LocationService service)
        {
            this.service = service;
        }
    }
}