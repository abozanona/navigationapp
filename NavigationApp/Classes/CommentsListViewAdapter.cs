using System.Collections.Generic;
using Android.Content;
using Android.Views;
using Android.Widget;

namespace NavigationApp.Classes
{
    class CommentsListViewAdapter : BaseAdapter<string>
    {
        private List<string> mItems;
        private Context mContext;
        public CommentsListViewAdapter(Context context, List<string> items)
        {
            mContext = context;
            mItems = items;
        }
        public override string this[int position]
        {
            get
            {
                return mItems[position];
            }
        }

        public override int Count
        {
            get
            {
                return mItems.Count;
            }
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View row = convertView;
            if (row == null)
            {
                row = LayoutInflater.From(mContext).Inflate(Resource.Layout.CommentsListView, null, false);
            }
            TextView commentText = row.FindViewById<TextView>(Resource.Id.commentText);
            commentText.Text = mItems[position];
            return row;
        }
    }
}