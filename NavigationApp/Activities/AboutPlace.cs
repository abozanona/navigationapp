using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Org.Json;
using NavigationApp.Activities;
using Android.Graphics;
using System.Net;
using System.Threading.Tasks;

namespace NavigationApp.Classes
{
    [Activity(Label = "AboutPlace")]
    public class AboutPlace : Activity {
        int mPlaceId;

        #region Gellary
        List<string> _pictures;
        List<bool> isPictureDownloadedSuccesfully;
        Bitmap[] imageBitmap;
        int currentPic = 0;
        ImageView gellaryViewer;
        /// <summary>
        /// List of all images URL
        /// </summary>
        public List<string> pictures
        {
            set
            {
                gellaryViewer.SetImageResource(Android.Resource.Color.Transparent);
                if (value.Count == 0)
                {
                    gellaryViewer.Visibility = ViewStates.Gone;
                }
                else
                {
                    gellaryViewer.Visibility = ViewStates.Visible;
                    _pictures = value;
                    currentPic = 0;
                    imageBitmap = new Bitmap[value.Count];
                    isPictureDownloadedSuccesfully = new List<bool>(value.Count) { true };
                    //for (int i = 0; i < value.Count; i++)
                    //    imageBitmap[i] = GetImageBitmapFromUrl(value[i]);
                    displayPic();
                }
            }
            get { return _pictures; }
        }

        /// <summary>
        /// Display the current choosen picture
        /// </summary>
        void displayPic()
        {
            int place = currentPic;
            while (true)
            {
                if (currentPic < pictures.Count)
                    if (imageBitmap[currentPic] != null)
                        gellaryViewer.SetImageBitmap(imageBitmap[currentPic]);
                    else
                    {
                        if(isPictureDownloadedSuccesfully[currentPic])
                        {
                            gellaryViewer.SetImageResource(Android.Resource.Color.Transparent);
                        }
                        else
                        {
                            currentPic++;
                            currentPic %= pictures.Count;
                            if (currentPic == place)
                            {
                                gellaryViewer.Visibility = ViewStates.Gone;
                                return;
                            }
                        }
                    }
            }
        }
        /// <summary>
        /// Download images from the web
        /// </summary>
        /// <param name="url">image picture</param>
        /// <returns></returns>
        async Task<Bitmap> GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;
            try
            {
                using (var webClient = new WebClient())
                {
                    var imageBytes = await webClient.DownloadDataTaskAsync(url);
                    if (imageBytes != null && imageBytes.Length > 0)
                    {
                        //imageBitmap = BitmapFactory.DecodeByteArray(Encoding.ASCII.GetBytes(imageBytes), 0, imageBytes.Length);
                        imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                    }
                }
            }
            catch
            {
                //Silence is gold.
            }
            displayPic();
            return imageBitmap;
        }

        #endregion

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AboutPlace);

            //get Place Id
            int placeId = Intent.GetIntExtra("placeId", 0);
            if (placeId == 0)
            {
                StartActivity(typeof(Search));
                Finish();
                return;
            }
            mPlaceId = placeId;

            //send request to the server to get place information
            string url = Resources.GetString(Resource.String.server_url);
            Dictionary<string, string> d = new Dictionary<string, string>();
            d["action"] = "placeInformation";
            d["placeId"] = placeId + "";
            string content = JsonValue.FetchJson(url, d);
            content = @"{""placeName"":""Al-Aqsa mosq"", ""placeDescription"":""this is a description"",""placeRate"":3,""placeImages"":[""http://cdn.macrumors.com/article-new/2013/09/iphone5c-header.jpg"",""http://blogs-images.forbes.com/gordonkelly/files/2016/04/Screenshot-2016-04-04-at-16.13.45.png"",""https://www.officialiphoneunlock.co.uk/images/iphonese.png?v=3"",""http://cdn.macrumors.com/article-new/2013/09/green-iphone5c.jpg?retina"",""http://images.apple.com/iphone/home/images/social/og.jpg?201607281539"",""http://www.techgadgets.in/images/palringo-rms-iphone.jpg"",""http://store.storeimages.cdn-apple.com/4973/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone6/silver/iphone6-silver-select-2014_GEO_US?wid=470&hei=556&fmt=png-alpha&qlt=95&.v=cdYkN0"",""http://www.electrony.net/media/2016/03/iphone-se.jpg"",""http://store.storeimages.cdn-apple.com/4662/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphonese/box/iphonese-box-gold-2016_GEO_ES?wid=234&hei=496&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=L2IkV0"",""http://blogs-images.forbes.com/gordonkelly/files/2016/04/Screenshot-2016-04-04-at-16.12.35-1200x675.png""]}";

            //UI init
            TextView lblDescription = FindViewById<TextView>(Resource.Id.lbldescription);
            Button btnGoToComments = FindViewById<Button>(Resource.Id.btnGoToComments);
            RatingBar placeStars = FindViewById<RatingBar>(Resource.Id.placeStars);

            #region Gellary
            ImageButton leftArrow = FindViewById<ImageButton>(Resource.Id.leftArrow);
            ImageButton rightArrow = FindViewById<ImageButton>(Resource.Id.rightArrow);
            gellaryViewer = FindViewById<ImageView>(Resource.Id.gellaryViewer);
            #endregion

            //Gellary placeGellary = FindViewById<Gellary>(Resource.Id.placeGellary);

            //parsing request json
            JSONObject jObj;
            try
            {
                jObj = new JSONObject(content);
                string placeName = jObj.GetString("placeName");
                string placeDescription = jObj.GetString("placeDescription");
                int placeRate = jObj.GetInt("placeRate");
                JSONArray JplaceImages = jObj.GetJSONArray("placeImages");

                List<string> placeImages = new List<string>();
                for (int i = 0; i < JplaceImages.Length(); i++)
                    placeImages.Add(JplaceImages.GetString(i));

                this.Title = "About " + placeName + ":";
                lblDescription.Text = placeDescription;
                placeStars.Rating = placeRate;
                //placeGellary.pictures = placeImages;
                pictures = placeImages;
                for (int i = 0; i < pictures.Count; i++)
                {
                    imageBitmap[i] = await GetImageBitmapFromUrl(pictures[i]);
                    if (imageBitmap[i] == null)
                    {
                        isPictureDownloadedSuccesfully[i] = false;
                    }
                }
            }
            catch (JSONException)
            {
                Toast.MakeText(this, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
            }

            btnGoToComments.Click += BtnGoToComments_Click;

            //gellary prev and next buttons
            #region Gellary
            rightArrow.Click += delegate
            {
                currentPic++;
                currentPic %= pictures.Count;
                displayPic();
            };
            leftArrow.Click += delegate
            {
                currentPic--;
                if (currentPic == -1)
                    currentPic = pictures.Count - 1;
                currentPic %= pictures.Count;
                displayPic();
            };
            displayPic();
            #endregion
        }

        /// <summary>
        /// Loading comments activity
        /// </summary>
        private void BtnGoToComments_Click(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Comments));
            intent.PutExtra("placeId", mPlaceId);
            StartActivity(intent);
        }
    }
}