using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using NavigationApp.Classes;
using Org.Json;
using Android.Support.V7.App;

namespace NavigationApp.Activities
{
    /// <summary>
    /// A single comment
    /// </summary>
    class CommentItem
    {
       public CommentItem(string _comment, int _commentId, bool _isMyComment, string _userName)
        {
            comment = _comment;
            commentId = _commentId;
            isMyComment = _isMyComment;
            userName = _userName;
        }
        public string comment { set; get; }
        public string userName { set; get; }
        public int commentId { set; get; }
        public bool isMyComment;
        public override string ToString()
        {
            return comment;
        }
    }
    [Activity(Label = "Comments")]
    public class Comments : Activity
    {
        int getMoreTimes = 0;
        ListView lsvComments;
        List<CommentItem> CommentsList;
        Button commentsSeeMore;
        Button btnMakeComment;
        TextView txtMakeComment;
        RatingBar rtbMakeRate;
        bool noMoreResultsFromServer = false;

        /// <summary>
        /// For reporting a comment
        /// </summary>
        public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
        {
            if (v.Id == Resource.Id.lsvComments)
            {
                var info = (AdapterView.AdapterContextMenuInfo)menuInfo;
                menu.SetHeaderTitle("Report");
                menu.Add(Menu.None, 1, 1, "Report comment");
            }
        }

        /// <summary>
        /// For reporting a comment(Sending report request to the server)
        /// </summary>
        public override bool OnContextItemSelected(IMenuItem item)
        {
            var info = (AdapterView.AdapterContextMenuInfo)item.MenuInfo;
            int menuItemIndex = item.ItemId;

            if (menuItemIndex == 1)
            {
                int listPosition = info.Position;

                string url = Resources.GetString(Resource.String.server_url);
                Dictionary<string, string> d = new Dictionary<string, string>();
                d["action"] = "report";
                d["commentId"] = listPosition+"";
                string content = JsonValue.FetchJson(url, d);
                content = @"{""status"":true}";
                JSONObject jObj;
                try
                {
                    jObj = new JSONObject(content);
                    if (jObj.GetBoolean("status") == true)
                    {
                        Toast.MakeText(this, Resources.GetString(Resource.String.report_message), ToastLength.Long);
                    }
                    else
                    {
                        Toast.MakeText(this, Resources.GetString(Resource.String.report_failed), ToastLength.Long);
                    }
                }
                catch(JSONException)
                {
                    Toast.MakeText(this.ApplicationContext, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
                }


            }
            else if (menuItemIndex == Resource.Id.home)
            {
                Finish();
            }
            return true;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Comments);

            ActionBar.SetHomeButtonEnabled(true);

            //UI init
            lsvComments = FindViewById<ListView>(Resource.Id.lsvComments);
            commentsSeeMore = FindViewById<Button>(Resource.Id.commentsSeeMore);
            btnMakeComment = FindViewById<Button>(Resource.Id.btnMakeComment);
            txtMakeComment = FindViewById<TextView>(Resource.Id.txtMakeComment);
            rtbMakeRate = FindViewById<RatingBar>(Resource.Id.rtbMakeRate);

            commentsSeeMore.Visibility = ViewStates.Invisible;

            CommentsList = new List<CommentItem>();

            searchOnServer();
            
            commentsSeeMore.Click += delegate
            {
                searchOnServer();
            };

            btnMakeComment.Click += delegate
            {
                makeAComment();
            };
            RegisterForContextMenu(lsvComments);

            
        }

        /// <summary>
        /// Sending a request to the server to make a new comment
        /// </summary>
        void makeAComment()
        {
            string url = Resources.GetString(Resource.String.server_url);
            Dictionary<string, string> d = new Dictionary<string, string>();
            d["action"] = "makeComment";
            d["placeId"] = Intent.GetIntExtra("placeId", 0) + "";
            d["commentText"] = txtMakeComment.Text;
            d["commentRate"] = rtbMakeRate.Rating+"";
            d["userId"] = MainActivity.userId;
            string response = JsonValue.FetchJson(url, d);
            
            JSONObject responseState = new JSONObject(response);
            try
            {
                if (responseState.GetBoolean("status") == false)
                {
                    Toast.MakeText(this, Resources.GetString(Resource.String.comment_posting_failed), ToastLength.Long).Show();
                }
                else
                {
                    CommentsList.Add(new CommentItem(d["commentText"], 0, true, MainActivity.userName));
                    var adabter = new ArrayAdapter<CommentItem>(this, Android.Resource.Layout.SimpleListItem1, CommentsList);
                    lsvComments.Adapter = adabter;
                }
            }
            catch (JSONException)
            {
                Toast.MakeText(this.ApplicationContext, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
            }
        }

        //load comments from the server
        void searchOnServer()
        {
            string url = Resources.GetString(Resource.String.server_url);
            Dictionary<string, string> d = new Dictionary<string, string>();
            d["action"] = "getComments";
            d["placeId"] = Intent.GetIntExtra("placeId", 0) + "";
            d["limit"] = getMoreTimes + "";
            string content = JsonValue.FetchJson(url, d);
            content = @"{""noMoreResultsFromServer"":true,""newcontent"":[{""commentText"":""aaaaaaaa"",""commentId"":1,""isMyComment"":false},{""commentText"":""bbbbbb"",""commentId"":2,""isMyComment"":false},{""commentText"":""ccccccccc"",""commentId"":3,""isMyComment"":true}]}";
            /**********************/
            //Format Needs Update
            //content = "{\"noMoreResultsFromServer\":\"yes\",\"newcontent\":[[\"aaaaaa\",1],[\"sssssssss\",2],[\"dddddddddddd\",3]]}";
            /**********************/
            JSONObject jObj;
            try
            {
                jObj = new JSONObject(content);
            }
            catch(JSONException)
            {
                Toast.MakeText(this.ApplicationContext, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
                jObj = new JSONObject("{\"noMoreResultsFromServer\":\"yes\",\"newcontent\":[]}");
            }

            updatelsvComments(jObj);
        }

        //load comments from the server(On see more button click)
        void updatelsvComments(JSONObject searchResult)
        {
            getMoreTimes++;
            
            try
            {
                if (searchResult != null)
                {
                    noMoreResultsFromServer = searchResult.GetString("noMoreResultsFromServer") == "yes" ? true : false;

                    var jArr = new JSONArray(searchResult.GetString("newcontent"));
                    for (int i = 0; i < jArr.Length(); i++)
                    {
                        var item = jArr.GetJSONObject(i);
                        CommentsList.Add(new CommentItem(
                            item.GetString("commentText"),
                            item.GetInt("commentId"),
                            item.GetBoolean("isMyComment"),
                            item.GetString("userName")
                            ));
                    }
                }
            }
            catch (JSONException)
            {
                Toast.MakeText(this.ApplicationContext, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
            }

            if (noMoreResultsFromServer)
            {
                commentsSeeMore.Visibility = ViewStates.Invisible;
            }
            else
            {
                commentsSeeMore.Visibility = ViewStates.Visible;
            }

            var adabter = new ArrayAdapter<CommentItem>(this, Android.Resource.Layout.SimpleListItem1, CommentsList);
            lsvComments.Adapter = adabter;
        }

    }
}