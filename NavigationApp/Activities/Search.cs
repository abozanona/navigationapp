using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Org.Json;
using NavigationApp.Classes;
using Android.Content;
using Android.Views.InputMethods;
using Android.Preferences;

namespace NavigationApp
{
    /// <summary>
    /// single search item
    /// </summary>
    class SearchResultItem
    {
        public SearchResultItem(string _result, int _resultId)
        {
            result = _result;
            resultId = _resultId;
        }
        public string result { set; get; }
        public int resultId { set; get; }
        public override string ToString()
        {
            return result;
        }
    }

    [Activity(Label = "Search")]
    public class Search : ActivityWithMenu
    {
        int getMoreTimes = 0;
        ListView lsvSearch;
        List<SearchResultItem> searchResults;
        Button btnSearch, btnSearchSeemore;
        EditText txtSearch;
        bool noMoreResultsFromServer = true;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Search);

            //UI init
            btnSearch = FindViewById<Button>(Resource.Id.btnsearch);
            txtSearch = FindViewById<EditText>(Resource.Id.txtSearch);
            btnSearchSeemore = FindViewById<Button>(Resource.Id.btnSearchSeemore);
            lsvSearch = FindViewById<ListView>(Resource.Id.lsvSearch);
            searchResults = new List<SearchResultItem>();

            // Create your application here

            btnSearch.Click += delegate
            {
                searchOnServer();
            };
            
            lsvSearch.ItemClick += LsvSearch_ItemClick;

            if (noMoreResultsFromServer)
            {
                btnSearchSeemore.Visibility = ViewStates.Invisible;
            }
            else
            {
                btnSearchSeemore.Visibility = ViewStates.Visible;
            }
            txtSearch.EditorAction += TxtSearch_EditorAction;
        }

        /// <summary>
        /// ON keyboard search button click,
        /// Searching the server for the entered query
        /// </summary>
        private void TxtSearch_EditorAction(object sender, TextView.EditorActionEventArgs e)
        {
            if (e.ActionId == ImeAction.Search)
            {
                searchOnServer();
                //e.handled = true;
            }
        }

        /// <summary>
        /// When clicking on a search result(place), open that place in new activity
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LsvSearch_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            
            Intent resultIntent = new Intent(this, typeof(AboutPlace));
            resultIntent.PutExtra("placeId", searchResults[e.Position].resultId);

            StartActivity(resultIntent);

        }

        /// <summary>
        /// sending the search query to the server and recieveng the result
        /// </summary>
        void searchOnServer()
        {
            //get saves settings
            ISharedPreferences sp = PreferenceManager.GetDefaultSharedPreferences(this.ApplicationContext);
            string settings = sp.GetString("preferredSettings", "{}");


            string url = Resources.GetString(Resource.String.server_url);
            Dictionary<string, string> d = new Dictionary<string, string>();
            d["action"] = "search";
            d["searchQuery"] = txtSearch.Text;
            d["limit"] = getMoreTimes + "";
            d["settings"] = settings;
            
            string content = JsonValue.FetchJson(url, d);
            content = @"{""noMoreResultsFromServer"":true, ""newcontent"":[{""text"":""111111"",""placeId"":1},{""text"":""222222"",""placeId"":2},{""text"":""333333"",""placeId"":3},{""text"":""44444444"",""placeId"":4},]}";
            JSONObject jObj;
            try
            {
                jObj = new JSONObject(content);
            }
            catch(JSONException)
            {
                Toast.MakeText(this.ApplicationContext, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
                jObj = new JSONObject("{\"noMoreResultsFromServer\":\"yes\",\"newcontent\":[]}");
            }

            updatelsvSearch(jObj, false);
        }

        /// <summary>
        /// updating the search results listView
        /// </summary>
        /// <param name="searchResult">Search results</param>
        /// <param name="isSeeMore">Are there more results for the next search</param>
        void updatelsvSearch(JSONObject searchResult, bool isSeeMore)
        {
            if (!isSeeMore)
            {
                searchResults.Clear();
                getMoreTimes = 0;
            }
            else
            {
                getMoreTimes++;
            }
                
            try
            {
                if (searchResult != null)
                {
                    noMoreResultsFromServer = searchResult.GetString("noMoreResultsFromServer") == "yes" ? true : false;

                    var jArr = new JSONArray(searchResult.GetString("newcontent"));
                    for (int i = 0; i < jArr.Length(); i++)
                    {
                        var item = jArr.GetJSONObject(i);
                        searchResults.Add(new SearchResultItem(
                            item.GetString("text"),
                            item.GetInt("placeId")
                            ));
                    }
                }
            }
            catch(JSONException)
            {
                Toast.MakeText(this.ApplicationContext, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
            }

            if (noMoreResultsFromServer)
            {
                btnSearchSeemore.Visibility = ViewStates.Invisible;
            }
            else
            {
                btnSearchSeemore.Visibility = ViewStates.Visible;
            }

            var adabter = new ArrayAdapter<SearchResultItem>(this, Android.Resource.Layout.SimpleListItem1, searchResults);
            lsvSearch.Adapter = adabter;
        }
    }
}