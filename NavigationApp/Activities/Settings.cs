using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Org.Json;
using Android.Preferences;
using Java.Lang;

namespace NavigationApp
{
    [Activity(Label = "Settings")]
    public class Settings : ActivityWithMenu {
        Button save;

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Settings);
            // Create your application here
            fillSettings();
            save = FindViewById<Button>(Resource.Id.save);

            //saving new settings
            save.Click += delegate {
                JSONObject preferredSettings = new JSONObject();
                preferredSettings.Put("searchRange", Integer.ParseInt(FindViewById<EditText>(Resource.Id.searchRange).Text));
                preferredSettings.Put("Banks", FindViewById<CheckBox>(Resource.Id.chkBanks).Checked);
                preferredSettings.Put("Hospitals", FindViewById<CheckBox>(Resource.Id.chkHospitals).Checked);
                preferredSettings.Put("Historicalplaces", FindViewById<CheckBox>(Resource.Id.chkHistoricalplaces).Checked);

                ISharedPreferences sp = PreferenceManager.GetDefaultSharedPreferences(Application.Context);
                ISharedPreferencesEditor editor = sp.Edit();
                editor.PutBoolean("issettingsset", true);
                editor.PutString("preferredSettings", preferredSettings.ToString());
                editor.Apply();
                StartActivity(typeof(Search));
                Finish();
            };
        }

        /// <summary>
        /// Loading saved settings
        /// </summary>
        void fillSettings() {
            ISharedPreferences sp = PreferenceManager.GetDefaultSharedPreferences(this.ApplicationContext);
            string str = sp.GetString("preferredSettings", "{}");

            try {
                JSONObject preferredSettings = new JSONObject(str);
                FindViewById<EditText>(Resource.Id.searchRange).Text = preferredSettings.GetString("searchRange");
                FindViewById<CheckBox>(Resource.Id.chkBanks).Checked = preferredSettings.GetBoolean("Banks");
                FindViewById<CheckBox>(Resource.Id.chkHospitals).Checked = preferredSettings.GetBoolean("Hospitals");
                FindViewById<CheckBox>(Resource.Id.chkHistoricalplaces).Checked = preferredSettings.GetBoolean("Historicalplaces");
            } catch (JSONException) {
                ;
            }
        }
    }
}