﻿using Android.App;
using Android.Content;
using Android.Runtime;
using Android.OS;
using Xamarin.Facebook;
using Xamarin.Facebook.Login.Widget;
using Android.Preferences;
using NavigationApp.Classes;
using Android.Widget;
using System.Collections.Generic;
using Org.Json;

namespace NavigationApp
{
    [Activity(Label = "NavigationApp", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : ActivityWithMenu, IFacebookCallback {
        
        //name used for posting commints
        public static string userId;
        public static string userName;
        private ICallbackManager mcallbackmanager;

        public void OnCancel() {
        }

        public void OnError(FacebookException error) {
        }

        /// <summary>
        /// method claaed when user logges in with facebook
        /// </summary>
        /// <param name="result"></param>
        public void OnSuccess(Java.Lang.Object result) {
            checkLogin();
        }

        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            FacebookSdk.SdkInitialize(this.ApplicationContext);
            SetContentView(Resource.Layout.Main);

            new DatabaseConnection();

            App.StartLocationService();

            checkLogin();

            LoginButton button = FindViewById<LoginButton>(Resource.Id.login_button);

            mcallbackmanager = CallbackManagerFactory.Create();
            button.RegisterCallback(mcallbackmanager, this);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data) {
            base.OnActivityResult(requestCode, resultCode, data);
            mcallbackmanager.OnActivityResult(requestCode, (int)resultCode, data);
            checkLogin();
        }

        /// <summary>
        /// check if user is already logged-in
        /// </summary>
        private void checkLogin() {

            if (AccessToken.CurrentAccessToken != null) {
                Dictionary<string, string> d=new Dictionary<string, string>();
                d["action"] = "register";
                d["userId"] = Profile.CurrentProfile.Id;
                d["userName"] = Profile.CurrentProfile.Name;
                string responce = JsonValue.FetchJson(Resources.GetString(Resource.String.server_url), d);
                responce = @"{""status"":true}";
                try
                {
                    JSONObject obj = new JSONObject(responce);
                    if (obj.GetBoolean("status") == true)
                    {
                        userId = Profile.CurrentProfile.Id;
                        userName = Profile.CurrentProfile.Name;
                        ISharedPreferences sp = PreferenceManager.GetDefaultSharedPreferences(this.ApplicationContext);
                        if (sp.GetBoolean("issettingsset", false))
                            StartActivity(typeof(Search));
                        else
                            StartActivity(typeof(Settings));
                        Finish();
                    }
                    else
                    {
                        Toast.MakeText(this, "Login/Registration Failed", ToastLength.Long).Show();
                    }
                }
                catch(JSONException)
                {
                    Toast.MakeText(this, Resources.GetString(Resource.String.json_parsing_error), ToastLength.Long);
                }
            }
        }
    }
}

