using Android.App;
using Android.OS;

namespace NavigationApp
{
    [Activity(Label = "About")]
    public class About : ActivityWithMenu {
        protected override void OnCreate(Bundle savedInstanceState) {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.About);

            // Create your application here
        }
    }
}